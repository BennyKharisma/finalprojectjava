Payment Receipt

1. ID: ST001
Food Name: Ikan Goreng
Price: 1
Quantity: 40000
Total Price: 40000
Payment Total: 50000
Money Changed: 10000

2. ID: ST002
Food Name: Ikan Goreng
Price: 2
Quantity: 40000
Total Price: 80000
Payment Total: 100000
Money Changed: 20000

3. ID: ST003
Food Name: Tahu Bulat
Price: 10
Quantity: 3000
Total Price: 30000
Payment Total: 33000
Money Changed: 3000

4. ID: ST004
Food Name: Tahu Bulat
Price: 10
Quantity: 3000
Total Price: 30000
Payment Total: 40000
Money Changed: 10000

