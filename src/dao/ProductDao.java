package dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import db.FoodDb;


public class ProductDao {

	Connection connection;
	
	public ProductDao() {
		try {
			initConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initConnection() throws SQLException {
		connection = FoodDb.connect();
		if(connection == null)
			throw new SQLException("Connection");
	}

	public Vector<Vector<String>> getCustomerData() {
		Vector<Vector<String>> data = new Vector<>();
		try {
			java.sql.Statement stmt = connection.createStatement();
			String sql = "select * from foodlist";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				Vector<String> rows = new Vector<>();
				rows.add(rs.getString(1));
				rows.add(rs.getString(2));
				rows.add(rs.getString(3));
				rows.add(rs.getString(4));
				data.add(rows);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	
	public void updateData(int sold, int stock, String id) {
		try {
			java.sql.Statement stmt = connection.createStatement();
			String sql = "update foodlist set food_quantity ='" + (stock-sold) +"' where food_id='"+id+"'";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getData(String name, int price, int quantity, int priceTotal, int payment, int moneyChanged) {
		try {
			java.sql.Statement stmt = connection.createStatement();
			String sql = "insert into foodhistory values('"+ getLatestID() +"', '"+name+"', '"+quantity+"', '"+price+"', '"+payment+"')";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private int getLatestID() {
		int id = 0, newId = 1;
		try {
			java.sql.Statement stmt = connection.createStatement();
			String sql = "select payment_id from foodhistory ORDER BY payment_id DESC LIMIT 1";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				id = Integer.parseInt(rs.getString(1));
			}
			newId = id + 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newId;
	}
	
	public void printReceipt() {
		    try {
		      FileWriter myWriter = new FileWriter("receipt.txt");
			     myWriter.write("Payment Receipt\n\n");
			     try {
						java.sql.Statement stmt = connection.createStatement();
						String sql = "select * from foodhistory";
						ResultSet rs = stmt.executeQuery(sql);
						int a = 1;
						while(rs.next()) {
					    	  myWriter.write(a + ". ID: ST00" + rs.getString(1) + "\n");
					    	  myWriter.write("Food Name: " + rs.getString(2) + "\n");
					    	  myWriter.write("Price: " + rs.getString(3) + "\n");
					    	  myWriter.write("Quantity: " + rs.getString(4) + "\n");
					    	  myWriter.write("Total Price: " + (Integer.parseInt(rs.getString(3)) * Integer.parseInt(rs.getString(4))) + "\n");
					    	  myWriter.write("Payment Total: " + rs.getString(5) + "\n");
					    	  myWriter.write("Money Changed: " + (Integer.parseInt(rs.getString(5)) - (Integer.parseInt(rs.getString(3)) * Integer.parseInt(rs.getString(4)))) + "\n\n");
					    	  a++;
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		      myWriter.close();
		    } catch (IOException e) {
		      e.printStackTrace();
		    }
	}
}
