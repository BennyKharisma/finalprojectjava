package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.ProductDao;

public class ListItem extends JFrame implements MouseListener, ActionListener{

	DefaultTableModel tableModel;
	JTable table;
	JButton printReceipt;
	
	public ListItem() {
;        initFrame();
	}

	private void initFrame() {
		setTitle("List Item");
        setSize(500, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(2, 0));
        setResizable(false);
        setLocation(200, 200);
		initJTable();
		JButtonPrint();
        setVisible(true);
    }
	
	private void initJTable() {	
		ProductDao productDao = new ProductDao();
		
        Vector<String> columns = new Vector<>();
        columns.add("Id");
        columns.add("Name");
        columns.add("Quantity");
        columns.add("Price");
        
        tableModel = new DefaultTableModel(productDao.getCustomerData(), columns) {
			@Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table = new JTable(tableModel);
        table.addMouseListener(this);
        JScrollPane sp = new JScrollPane(table);
        add(sp);
    }
	
	private void JButtonPrint() {
		printReceipt = new JButton("Print Receipt");
		printReceipt.addActionListener(this);
		add(printReceipt);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int selectRowIndex = table.getSelectedRow();
		new BuyItem(tableModel.getValueAt(selectRowIndex, 0).toString(), tableModel.getValueAt(selectRowIndex, 1).toString(),
				tableModel.getValueAt(selectRowIndex, 3).toString(), tableModel.getValueAt(selectRowIndex, 2).toString());
		setVisible(false);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(printReceipt)) {
			ProductDao productDAO = new ProductDao();
			productDAO.printReceipt();
			JOptionPane.showMessageDialog(null, "Success Print Receipt");
		}
		
	}

}
