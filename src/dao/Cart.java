package dao;

public class Cart {

	int ID;
	String Name;
	int Price;
	int Quantity;
	int PriceTotal;
	int Payment;
	int MoneyChanged;
	
	public Cart(int iD, String name, int price, int quantity, int priceTotal, int payment, int moneyChanged) {
		super();
		ID = iD;
		Name = name;
		Price = price;
		Quantity = quantity;
		PriceTotal = priceTotal;
		Payment = payment;
		MoneyChanged = moneyChanged;
	}
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		Price = price;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public int getPriceTotal() {
		return PriceTotal;
	}

	public void setPriceTotal(int priceTotal) {
		PriceTotal = priceTotal;
	}

	public int getPayment() {
		return Payment;
	}

	public void setPayment(int payment) {
		Payment = payment;
	}

	public int getMoneyChanged() {
		return MoneyChanged;
	}

	public void setMoneyChanged(int moneyChanged) {
		MoneyChanged = moneyChanged;
	}
	
	
}
