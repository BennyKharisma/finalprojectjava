package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.ProductDao;

public class BuyItem extends JFrame implements ActionListener{

	JTextField txtId;
    JTextField txtName;
    JTextField txtPrice;
    JTextField txtQuantity;
    JTextField txtMoney;
    Integer stock;
    JButton saveButton = new JButton("Save");
    JButton cancelButton = new JButton("Cancel");
	
	public BuyItem(String id, String name, String price, String quantity) {
		initFrame(id, name, price, quantity);
	}

	private void initFrame(String id, String name, String price, String quantity) {
        setTitle("Buy Item");
        setSize(300, 300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(0, 2));
        setResizable(false);
        initComponent(id, name, price, quantity);
        setLocation(200, 200);
        setVisible(true);
    }
	
	private void initComponent(String id, String name, String price, String quantity) {
        JLabel lblId = new JLabel("Id: ");
        txtId = new JTextField();
        txtId.setText(id);
        txtId.setEditable(false);
        JLabel lblName = new JLabel("Name: ");
        txtName = new JTextField();
        txtName.setText(name);
        txtName.setEditable(false);
        JLabel lblPrice = new JLabel("Price: ");
        txtPrice = new JTextField();
        txtPrice.setText(price);
        txtPrice.setEditable(false);
        JLabel lblQuantity = new JLabel("Quantity:  ");
        txtQuantity = new JTextField();
        stock = Integer.parseInt(quantity);
        JLabel lblTotal = new JLabel("Total Price:  ");
        JLabel lblMoney = new JLabel("Payment Total:  ");
        txtMoney = new JTextField();
        add(lblId);
        add(txtId);
        add(lblName);
        add(txtName);
        add(lblPrice);
        add(txtPrice);
        add(lblQuantity);
        add(txtQuantity);
        add(lblMoney);
        add(txtMoney);
        add(saveButton);
        add(cancelButton);
        saveButton.addActionListener(this);
        cancelButton.addActionListener(this);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(saveButton)) {
			ProductDao productDAO = new ProductDao();
			if(Integer.parseInt(txtQuantity.getText()) > stock) {
				JOptionPane.showMessageDialog(null, "Quantity exceed stock limit");
			}else if(Integer.parseInt(txtQuantity.getText()) * Integer.parseInt(txtPrice.getText()) > Integer.parseInt(txtMoney.getText())){
				JOptionPane.showMessageDialog(null, "Payment not enough to buy this items");
			}else {
				productDAO.updateData(Integer.parseInt(txtQuantity.getText()), stock, txtId.getText());
				productDAO.getData(txtName.getText(), Integer.parseInt(txtPrice.getText()) , Integer.parseInt(txtQuantity.getText()), (Integer.parseInt(txtQuantity.getText()) * Integer.parseInt(txtPrice.getText())), Integer.parseInt(txtMoney.getText()), (Integer.parseInt(txtMoney.getText()) - Integer.parseInt(txtQuantity.getText()) * Integer.parseInt(txtPrice.getText())));
				JOptionPane.showMessageDialog(null, "Success buy Item\n"
						+ "Total Price = "+ (Integer.parseInt(txtQuantity.getText()) * Integer.parseInt(txtPrice.getText())) + "\n"
								+ "Money Returned = " + (Integer.parseInt(txtMoney.getText()) - Integer.parseInt(txtQuantity.getText()) * Integer.parseInt(txtPrice.getText())));
			
			}
			setVisible(false);
			new ListItem();
        } else if (e.getSource().equals(cancelButton)) {
            setVisible(false);
            new ListItem();
        }
		
	}
	
}
